# server.py
import socket
# 1
s =  socket.socket()
host = socket.gethostname()
port = 4567
# 2
s.bind((host,port))
# 3
s.listen(1)
# 4
while True:
    conn, addr = s.accept()
    print('got connection from :',addr ) 
    conn.send(b'Thank for ur connecting') # b means byte sent a message
    conn.close()