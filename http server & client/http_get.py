from http.server import HTTPServer, BaseHTTPRequestHandler

task_list = ['Task 1','Task 2','Task 3']
class requestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(9)
        self.send_header('content-type', 'text/html')
        self.end_headers()
        output  = '<html><body>'
        output += '<h1>Task list</h1>'
        for task in task_list:
            output += task
            output += '</br>'
        output += '</body></html>'
        self.wfile.write(output.encode()),
            
        # end of the program


def main():
    Port = 8780
    server_address = ('localhost', Port)
    server = HTTPServer(server_address, requestHandler)
    print("Server running on Port %s" % Port)
    server.serve_forever()


if __name__ == "__main__":
    main()
