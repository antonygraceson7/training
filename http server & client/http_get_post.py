from http.server import HTTPServer, BaseHTTPRequestHandler
import cgi
task_list = ['Task 1', 'Task 2', 'Task 3']


class requestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path.endswith('/task'):
            self.send_response(200)
            self.send_header('content-type', 'text/html')
            self.end_headers()

            output = ""
            output += '<html><body>'
            output += '<h1>Task list</h1>'
            output += '<h3><a href="/task/new">Add a New Task list</a></h3>'

            for task in task_list:
                output += task
                output += '<a href="/task/%s/remove">X</a>' % task
                output += '</br>'
            output += '</body></html>'

            self.wfile.write(output.encode()),

        if self.path.endswith('/new'):
            self.send_response(200)
            self.send_header('content-type', 'text/html')
            self.end_headers()

            output = ""
            output += '<html><body>'
            output += '<h1>Add Task list</h1>'
            output += '<form method="POST" enctype="multipart/form-data" action="/task/new">'
            output += '<input name="task" type="text" placeholder="New Task">'
            output += '<input  type="submit" value="Add">'
            output += '</form>'
            output += '</body></html>'
            self.wfile.write(output.encode()),
            
            
        if self.path.endswith('/remove'):
            list_id_path = self.path.split('/')[2]
            self.send_response(200)
            self.send_header('content-type', 'text/html')
            self.end_headers()

            output = ""
            output += '<html><body>'
            output += '<h1>Remove Task: %s</h1>' % list_id_path.replace('%20','')
            output += '<form method="POST" enctype="multipart/form-data" action="/task/%s/remove">'% list_id_path
            output += '<input  type="submit" value="Remove">'
            output += '</form>'
            output += '<a href="/task">Cancel</a>'

            output += '</body></html>'
            self.wfile.write(output.encode()),    
        # end of the program

    def do_POST(self):
        if self.path.endswith('/new'):
            ctype,pdict = cgi.parse_header(self.headers.get('content-type'))
            print('ctype,pdict',ctype,pdict)
            pdict['boundary'] = bytes(pdict['boundary'], "utf-8")
            
            content_len = int(self.headers.get('content-length'))
            
            print("content_len",int(self.headers.get('content-length')))
            pdict['CONTENT-LENGTH'] = content_len
            print("content_len3",pdict['CONTENT-LENGTH'])
            if ctype == 'multipart/form-data':
                fields = cgi.parse_multipart(self.rfile, pdict)
                print("fields",self.rfile,'pdict',pdict)
                new_task = fields.get('task')
                print(new_task[0])
                task_list.append(new_task[0])
            self.send_response(301)
            self.send_header('content-type', 'text/html')
            self.send_header('Location', '/task')

            self.end_headers()
        if self.path.endswith('/remove'):
            list_id_path = self.path.split('/')[2]
            ctype,pdict = cgi.parse_header(self.headers.get('content-type'))
            if ctype == 'multipart/form-data':
                list_item = list_id_path.replace('%20','')
                task_list.remove(list_item)
            self.send_response(301)
            self.send_header('content-type', 'text/html')
            self.send_header('Location','/task')
            self.end_headers()

def main():
    Port = 8780
    server_address = ('localhost', Port)
    server = HTTPServer(server_address, requestHandler)
    print("Server running on Port %s" % Port)
    server.serve_forever()


if __name__ == "__main__":
    print('&&&&&&&&')
    main()
