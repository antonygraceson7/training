# Bubble sort method 1
def Bubble_sort(num):
    for i in range(len(num)):  # for i in range(len(num)-1,0,-1)
        for j in range(0,len(num)-1):  # for j in range(i)
            if num[j] > num[j+1]:
                temp = num[j]
                num[j] = num[j+1]
                num[j+1] = temp
            
num = [457, 67, 879, 657, 456, 78, 89]
Bubble_sort(num)
print(num)

# Bubble sort method 2
def bubble_sort(num):
    # print(range(len(num)-1,0,-1))
    for i in range(len(num)-1,0,-1):
        for j in range(i):  
            if num[j] > num[j+1]:
                temp = num[j]
                num[j] = num[j+1]
                num[j+1] = temp
            
num = [457, 67, 879, 657, 456, 78, 89]
bubble_sort(num)
print(num)