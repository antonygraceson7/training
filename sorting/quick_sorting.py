
# This function takes last element as pivot, places
# the pivot element at its correct position in sorted
# array,and places all smaller (smaller than pivot)
# to left of pivot and all greater elements to right

def partition(arr,low, high):
    i = (low-1)                  # index of smaller element
    pivot = arr[high]	         # pivot
    # print("low-1",i)
    # print("pivot",pivot)
    # print("range(low, high)",range(low, high))
    for j in range(low, high):
        # If current element is smaller than the pivot
        # print("condition",arr[j] < pivot)
        if arr[j] < pivot:
            i = i+1
            temp = arr[i]
            arr[i] = arr[j]
            arr[j] = temp

    temp = arr[i+1]
    arr[i+1] = arr[high]
    arr[high] = temp
    print('array', arr)
    return (i+1)

# The main function that implements QuickSort
# arr[] --> Array to be sorted,
# low --> Starting index [0], 
# high --> Ending index [list_last_value]

# Function to do Quick sort
def quickSort(arr, low, high):
    # print("low",low,"high",high)
    if low < high:
        # print(low,"<",high)
        # pi is partitioning index, arr[p] is now
        # at right place
        pi = partition(arr, low, high)
        print("pi",pi)

        # Separately sort elements before
        # partition and after partition
        quickSort(arr, low, pi-1)
        quickSort(arr, pi+1, high)


arr = [10,5,54,9,65,78,70]
     #  0 1 2  3  4  5  6
n = len(arr)
quickSort(arr, 0, n-1)
print ("Sorted array is:",arr)